#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging
import datetime

import pytz

from influxdb import InfluxDBClient

from benchsuite.core.model.storage import StorageConnector

logger = logging.getLogger(__name__)

class InfluxDBStorageConnector(StorageConnector):

    def __init__(self):
        self.influx_client = None
        self.influx_db_name = None

    def save_execution_error(self, exec_error):
        pass

    def save_execution_result(self, execution_result):

        # create a database, if not already exist
#        self.influx_client.create_database(self.influx_db)
        
        # switch to it
        self.influx_client.switch_database(self.influx_db)
        
        # create records
        records = self._create_records(execution_result)

        # store records
        self.influx_client.write_points(records)

    def _create_records(self, obj):

        record = {
            "measurement": obj.workload.tool,
            "tags": {
                "workload": obj.workload.bsid,
                "workload.tool": obj.workload.tool,
                "workload.name": obj.workload.name,
                "workload.version": obj.workload.version,
                "provider.default": obj.provider.bsid,
                "execution-id": obj.id
            },
            "time": datetime.datetime.fromtimestamp(obj.status.steps['run'].start, tz=pytz.utc)
        }

        record['tags'].update({f'execenv.default.{k}': v for k, v in obj.status.exec_envs['provider.default'].tags.items()})
        record['tags']['execenv.default'] = obj.status.exec_envs['provider.default'].bsid
        record['tags'].update({f'provider.default.{k}': v for k, v in obj.provider.tags.items()})

        # add metrics
        fields = {}
        for k, v in obj.status.metrics.items():
            if v['unit']:
                name = f'{k}_{v["unit"]}'
            else:
                name = k
            value = v['value'] + 0.0
            fields[name] = value
        record['fields'] = fields

        #if obj.performance:
        #    record['fields']['performance-index'] = obj.performance+0.0

        # add running node
        # check if we have the nodes where the components ran
        if obj.status.sut_nodes:
            if len(obj.status.sut_nodes) == 1:
                record['tags']['node'] = obj.status.sut_nodes[0]
            else:
                logger.warning('len(sut_nodes) != 1. Cannot determine the tag "node" value')

        res = [record]
        res.extend(self.__create_profiles_records(obj))

        # add scope information
        scope = "nobody"
        if obj.session.props.get('reports-scope', None):
            scope = obj.session.props["reports-scope"]
        elif obj.session.props.get('executor', None):
            executor = obj.session.props["executor"]
            scope = "usr:"+str(executor)

        for r in res:
            r['tags']['scope'] = scope

        logger.debug('InfluxDB record: [measurement: "%s", tags: %s]', res[0]['measurement'], res[0]['tags'])

        return res

    def __create_profiles_records(self, obj):
        res = []
        x =  obj.status.get_all_profiles()
        for profile in obj.status.get_all_profiles():
            for sample in profile['items']:
                if 'time' not in sample:
                    continue
                record = {
                    "measurement": 'Profile',
                    "tags": {
                        "workload": obj.workload.bsid,
                        "workload.tool": obj.workload.tool,
                        "workload.name": obj.workload.name,
                        "workload.version": obj.workload.version,
                        "provider.default": obj.provider.bsid,
                        "execution-id": obj.id,
                        "start-time": profile['t0']
                    },
                    'fields': dict(sample),
                    "time": int(sample['time']) * 1000000,  # ms to ns
                }

                record['tags'].update(
                    {f'execenv.default.{k}': v for k, v in obj.status.exec_envs['provider.default'].tags.items()})
                record['tags']['execenv.default'] = obj.status.exec_envs['provider.default'].bsid
                record['tags'].update({f'provider.default.{k}': v for k, v in obj.provider.tags.items()})
                record['tags'].update(profile['tags'])
                del record['fields']['time']
                res.append(record)
        return res

    @staticmethod
    def load_from_config(config, section):

        logger.debug('Loading %s', InfluxDBStorageConnector.__module__ + "." + __class__.__name__)

        o = InfluxDBStorageConnector()

        username = None
        if config.has_option(section, "username"):
          username = config.get(section, "username")

        password = None
        if config.has_option(section, "password"):
          password = config.get(section, "password")

        o.influx_client = InfluxDBClient(config.get(section, "hostname")
            , port=config.get(section, "port")
            , username=username
            , password=password)

        o.influx_db = config.get(section, "db_name")

        return o


