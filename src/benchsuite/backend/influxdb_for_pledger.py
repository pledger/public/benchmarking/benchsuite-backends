#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging
from influxdb import InfluxDBClient

from benchsuite.backend.influxdb import InfluxDBStorageConnector

logger = logging.getLogger(__name__)


class InfluxDBStorageConnectorForPledger(InfluxDBStorageConnector):

    def _create_records(self, obj):

        records = super()._create_records(obj)
        provider = obj.provider

        for r in records:
            r['tags']['pledger-infrastructure'] = provider.metadata.get('pledger-infrastructure', None)
            r['tags']['pledger-project'] = provider.metadata.get('pledger-project', None)
            r['tags']['pledger-service-provider'] = provider.metadata.get('pledger-service-provider', None)
            if 'node' in r['tags']:
                r['tags']['pledger-infrastructure-node'] = \
                    provider.metadata.get('pledger-infrastructure-node-ids', {}).get(r['tags']['node'], None)

        return records

    @staticmethod
    def load_from_config(config, section):

        logger.debug('Loading %s', InfluxDBStorageConnector.__module__ + "." + __class__.__name__)

        o = InfluxDBStorageConnectorForPledger()

        username = None
        if config.has_option(section, "username"):
          username = config.get(section, "username")

        password = None
        if config.has_option(section, "password"):
          password = config.get(section, "password")

        o.influx_client = InfluxDBClient(config.get(section, "hostname")
            , port=config.get(section, "port")
            , username=username
            , password=password)

        o.influx_db = config.get(section, "db_name")

        return o