#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging

import configparser

import datetime
import pytz
from pymongo import MongoClient
from benchsuite.core.model.storage import StorageConnector

logger = logging.getLogger(__name__)


class MongoDBStorageConnector(StorageConnector):

    def __init__(self):
        self.client = None
        self.db = None
        self.collection = None
        self.err_collection = None
        self.store_logs = False

    def save_execution_error(self, exec_descr_obj):
        r = self.err_collection.insert_one(self.__create_record(exec_descr_obj))
        logger.info('Execution error saved with with id=%s', r.inserted_id)

    def save_execution_result(self, execution_result):
        r = self.collection.insert_one(self.__create_record(execution_result))
        logger.info('New execution results stored with id=%s', r.inserted_id)

    def __create_record(self, obj):

        record = {}

        # NOTE: in order to this fields to be available in the REST/GUI responses, the REST models need to be updated!
        record.update({
            'status': obj.status.dict(),
            #'starttime': datetime.datetime.fromtimestamp(obj.status.steps_status['run'].start, tz=pytz.utc),
            'workload': {
                'tool': obj.workload.tool,
                'name': obj.workload.name,
                'wid': obj.workload.bsid,
                'id': obj.workload.id,
                'version': obj.workload.version
            },
            'provider': {
                'name': obj.provider.name,
                'id': obj.provider.id,
                'size': 'n/a'
            },
            'properties': obj.session.props,
        })

        return self._sanitize_record_rec(record)

    def _sanitize_record_rec(self, record):
        res = {}
        for k, v in record.items():
            new_v = v
            if type(v) is dict:
                new_v = self._sanitize_record_rec(v)
            if '.' in k:
                res[k.replace('.','_dot_')] = new_v
            else:
                res[k] = new_v
        return res

    @staticmethod
    def load_from_config(config, section):

        # define some defaults
        if 'error_collection_name' not in config[section]:
            config[section]['error_collection_name'] = 'exec_errors'

        if 'store_logs' not in config[section]:
            config[section]['store_logs'] = "False"

        if 'store_metrics' not in config[section]:
            config[section]['store_metrics'] = "False"

        logger.debug('Loading %s', MongoDBStorageConnector.__module__ + "." + __class__.__name__)

        o = MongoDBStorageConnector()
        o.client = MongoClient(config[section]['connection_string'])
        o.db = o.client[config[section]['db_name']]
        o.collection = o.db[config[section]['collection_name']]
        o.err_collection = o.db[config[section]['error_collection_name']]
        
        if config[section]['store_logs'].lower() in ('true', 'yes', 'y', '1'):
            o.store_logs = True
        else:
            o.store_logs = False

        if config[section]['store_metrics'].lower() in ('true', 'yes', 'y', '1'):
            o.store_metrics = True
        else:
            o.store_metrics = False
            
        logger.info('MongoDBStorageConnector created for %s, db=%s, coll=%s', config[section]['connection_string'], config[section]['db_name'], config[section]['collection_name'])

        return o


