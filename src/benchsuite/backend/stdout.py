#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging

import datetime
import pytz

from benchsuite.core.cli.ascii_tables import fmt_execution_describe
from benchsuite.core.model.storage import StorageConnector

logger = logging.getLogger(__name__)


class StdOutStorageConnector(StorageConnector):

    def save_execution_error(self, exec_error):
        print(exec_error)
        print(exec_error.__dict__)

    def save_execution_result(self, execution_result):
        fmt_execution_describe(execution_result, show_logs=False)

    @staticmethod
    def load_from_config(config, section):
        o = StdOutStorageConnector()
        return o


